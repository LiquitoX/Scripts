#!/bin/bash
# This script will install a set of packages that I use

###########################################
# TODO:
# - Add the following downloads to the
#   installer script too:
#   - http://s3.amazonaws.com/Minecraft.Download/launcher/Minecraft.jar
#   - ...
# - Rewrite installer script.
#   - Add Fedora support.
# - Add alias.sh to this installer script.
#   - Add aliases to the alias script.
###########################################

# Import functions from files/functions.sh
. ./files/functions.sh

# Check if the user is root.
checkRoot install.sh

# Set the current time so we can echo out the time taken for our script.
setTime

# Ask the user if he is installing a Virtual Machine, or a normal machine.
userInput installVM installPC "Is this system a Virtual Machine or a PC? Type Y for VM and N for PC."

# TODO: Add alias.sh moving, add mc downloader.

# End of script.
