#!/bin/bash
#####################################################
# The space below should only be used for declaring
# functions for all the other scripts.
#####################################################
apt=`command -v apt-get`
dnf=`command -v dnf`
#####################################################
# This function will check if the user agrees on a 
# predefined question (preferrably before you call
# this function).
#
# This function requires two inputs. 
# $1: Execute when the user agrees.
# $2: Execute when the user disagrees.
# $3: The question itself.
#####################################################
function userInput {
  read -p "$3 (y/N)" -n 1 -r
  if [[ $REPLY =~ ^[Yy]$ ]]
  then
    $1
  else 
    $2
  fi
}

#####################################################
# This function should be self-explanatory
#####################################################
function quit {
  echo 
  echo "Script will now exit!"
  exit
}

#####################################################
# This function will set the variable timeStart
#####################################################
function setTime {
  # Start the script with the current time.
  timeStart=`date +%s`
}

#####################################################
# This function will update Ubuntu and Fedora 
# based systems.
#####################################################
function updateSystem {
  clear
  userInput "" quit "This sytem will now be updated. Continue?"
  echo
  if [ -n "$apt" ]; then
    echo "This is an Ubuntu based system!"
    sudo apt-get update
    sudo apt upgrade -y
    sudo apt-get dist-upgrade -y
    sudo apt-get autoremove -y
  elif [ -n "$dnf" ]; then
    echo "This is a Fedora based system!"
    sudo dnf upgrade -y
    sudo dnf autoremove -y
  else
    echo "Error: no path to apt-get or dnf" >&2;
    exit 1;
  fi
  calculateTimeTaken
}

#####################################################
# This function will calculate the time taken by the
# action you provided earlier. This will also output
# a summary of the system.
#####################################################
function calculateTimeTaken {
  # Get the Epoch Time again
  timeEnd=`date +%s`

  # Calculate the time that was needed for the update
  timeTaken=`expr $timeEnd - $timeStart`

  # Clear the screen
  clear

  # Echo out the total time taken with a summary of the system
  echo $timeTaken" Seconds were needed for this script."
  screenfetch
}

#####################################################
# This function will install tools that I mostly 
# need on my own Virtual machines, like guest-agents
# and simple networking/editing tools
#####################################################
function installVM {
  packages="qemu-guest-agent tmux htop screenfetch net-tools cockpit cockpit-pcp"
  clear
  echo "This script will install some pre-chosen packages"
  echo "to be installed on this system."
  echo "These packages are:"
  echo $packages
  echo 
  echo 'Determining OS!'
  userInput "" quit "This script will now install the packages for a Virtual Machine. Continue?"
  # Check the package managers.
  if [ -n "$apt" ]; then
    echo "This is an Ubuntu based system!"
    apt-get install $packages
  elif [ -n "$dnf" ]; then
    echo "This is a Fedora based system!"
    dnf install $packages
  else
    echo "Error: no path to apt-get or dnf" >&2;
    exit 1;
  fi
  calculateTimeTaken
}

#####################################################
# This function will install tools that I mostly 
# need on my own Computers or laptops, like Qemu
# and simple networking/editing tools
#####################################################
function installPC {
  clear
  echo "This script will install some pre-chosen packages"
  echo "to be installed on this system."
  echo 
  echo 'Determining OS!'
  userInput "" quit "This script will now install the packages for a PC. Continue?"
  clear
  packages="smartmontools screenfetch net-tools tmux tlp qemu samba virt-manager rdesktop powertop"
  # Check the package managers.
  if [ -n "$apt" ]; then
    echo "This is an Ubuntu based system!"
    apt-get install $packages
  elif [ -n "$dnf" ]; then
    echo "This is a Fedora based system!"
    dnf install $packages
  else
    echo "Error: no path to apt-get or dnf" >&2;
    exit 1;
  fi
  calculateTimeTaken
}
#####################################################
# This function will  check if you are a root user.
#####################################################
function checkRoot {
  if [ "$(id -u)" != "0" ]; then
    echo "This script must be run as root. Please run 'sudo ./$1'"
    quit
  fi
}

function getDevPackage {
  clear
  userInput "" quit "This will now download PhpStorm, PyCharm and Rider!"
  echo ""
  echo "File will be stored at /home/"$USER"/Download/devPack.tar.gz!"
  sleep 2
  wget -O /home/$USER/Downloads/devPack.tar.gz https://www.welizmusic.com/files/IntellijPack.tar.gz
  quit
}

# End of script.
