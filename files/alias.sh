#!/bin/bash
# TODO
# - Add the following file to the userdirectory with a
#   symbolic link.
###############################################################
alias eu1='ssh wenzel@ssh.welizmusic.com'
alias ls='ls -l --color=tty'
alias cls='clear'
alias logoff='kill 9 -1'
alias addCommit="/home/$USER/Scripts/addCommit.sh"
alias fSize='/home/$USER/Scripts/fSize.sh'
alias diskStatus='/home/$USER/Scripts/diskStatus.sh'
