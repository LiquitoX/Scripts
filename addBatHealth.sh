#!/bin/bash

# This script will add the output from another script (bat.sh)
# into a file called BatHealth.txt which is made in the users
# home directory. The layout will always be:
# $date capacity %% capacity %%
# This stands for the first and second battery respectively.

# Declare filename
fileName="/home/"$USER"/BatHealth.txt"

# Run the script with a grep command and append it to the file.
echo "File will be created/appended in "$fileName
bat="`/home/$USER/Scripts/bat.sh | grep cap`"
date="`date`"
message=`echo $date" "$bat" Health"`
echo $message >> $fileName

# Output the complete file after completion.
cat $fileName

# End of script
