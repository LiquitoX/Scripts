#!/bin/bash

# Clear screen for readability
clear

# Get files to be added
filePath=${1}

# Get commit message
message=${2}

# Check if filepath is empty
if [ -z "$filePath" ]; then
  echo "No path given, exiting..."
  exit
fi

# Add the files to the current working tree
echo "Adding \"$filePath\" to the working tree."
sleep 1
git add $filePath

# Check if the commitMessage is empty
if [ -z "$message" ]; then
  # Prompt the user to enter a message now
  git commit
else
  # Add the commit message
  echo "Committing commit message: $message"
  git commit -m "$message"
fi


# Push the changes
echo "Pushing changes."
git push
