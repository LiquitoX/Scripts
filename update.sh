#!/bin/bash
# Updatescript by LiquitoX - 2017

# Clear the screen for readability.
clear

# Import functions from ./files/functions.sh
. ./files/functions.sh

# Check if the user is root.
checkRoot update.sh

# Set the current time.
setTime

# Run the actual script.
updateSystem

# End of Script
