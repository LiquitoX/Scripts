#!/bin/zsh
#
# This script is made to text and look at the statusses of disks that are attached
# to the internal raid controller of a server that I use.
#
# The /dev/sda part is not important for this controller. The $i part is.
for i in 0 1 2 3 4 5 6 7 8 9
do
  # Write the disk number.
  echo "";
  echo "Disk $i";

  # Run long test.
  if [ $1 = "-tl" ]; then
    smartctl /dev/sda -t long -d cciss,$i;
  fi

  # Run short test.
  if [ $1 = "-ts" ]; then
    smartctl /dev/sda -t short -d cciss,$i;
  fi

  # Get current test status.
  if [ $1 = "-t" ]; then
    smartctl /dev/sda -a -d cciss,$i | grep -e "Device Model" -e "User Capacity" -e "# 1 ";
  fi

  # Get general device information.
  if [ $1 = "-a" ]; then
    smartctl /dev/sda -a -d cciss,$i | grep -e "Device Model" -e "User Capacity" -e "Serial" -e "9 P";
  fi
done

# End of file.