#!/bin/bash

# Get the argument from the user.
path=${1}

# Check if the argument is empty.
if [ -z "$path" ]; then
  # Set to local folder/
  path=`pwd`
fi

# Get filesizes of $path
fileSize=`du $path -sh`

# Print the values to the terminal.
echo "Requested folder: "$path
echo "The size of the folder is: "$fileSize

# End of file.

