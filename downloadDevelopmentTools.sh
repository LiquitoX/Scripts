#!/bin/bash

# This function will check for dialog on the system.
function checkDialog {
  echo "This script needs dialog installed on your machine."
  echo "This script will now check and install if this is needed."
  installed=`program_is_installed dialog`
  if [ $installed == 0 ]; then
    echo "Dialog is not installed, this script will now run 'sudo apt install dialog'."
    sudo apt install dialog -y
    clear
    echo "Dialog was succesfully installed!"
    sleep 1
  else
    echo "Dialog is installed, continuing!"
    sleep 1
  fi
}

function program_is_installed {
  # set to 1 initially
  local return_=1
  # set to 0 if not found
  type $1 >/dev/null 2>&1 || { local return_=0; }
  # return value
  echo "$return_"
}

# This function makes the user choose between programs to install on his/her system.
function installDevTools {
  # Move to Documents folder first.
  folderPath="/home/$USER/Documents/Development"
  if [ ! -d "$folderPath" ]; then
    echo "Development folder not found, creating the folder!"
    mkdir "$folderPath"
  else
    echo "Development folder found, going on."
  fi
  cd $folderPath

  # Show the dialog
  cmd=(dialog --separate-output --checklist "Select packages to install:" 10 35 16)
  options=(1 "PhpStorm" off
           2 "PyCharm" off
           3 "Rider" off)
  choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)
  echo $choices
  clear
  echo "" > alias.sh
  for choice in $choices
  do
    case $choice in
    1)
      clear
      echo "Phpstorm will now be downloaded and installed."
      sleep 1
      wget -O "phpStorm.tar.gz" "http://download.jetbrains.com/webide/PhpStorm-2018.1.6.tar.gz"
      tar -zxvf ./phpStorm.tar.gz
      mv ./PhpStorm-181.5281.35 ./PhpStorm
      clear
      echo "PhpStorm successfully installed!"
      sleep 1
      ;;
    2)
      clear
      echo "PyCharm will now be downloaded and installed."
      sleep 1
      wget -O "pyCharm.tar.gz" "https://download.jetbrains.com/python/pycharm-professional-2018.1.5.tar.gz"
      tar -zxvf ./pyCharm.tar.gz
      mv ./pycharm-2018.1.5 ./PyCharm
      clear
      echo "PyCharm succesfully installed!"
      sleep 1
      ;;
    3)
      clear
      echo "Rider will now be downloaded and installed."
      sleep 1
      wget -O "rider.tar.gz" "https://download.jetbrains.com/rider/JetBrains.Rider-2018.1.4.tar.gz"
      tar -zxvf ./rider.tar.gz
      mv ./JetBrains\ Rider-2018.1.4/ ./Rider
      clear
      echo "Rider succesfully installed!"
      sleep 1
      ;;
    esac
  done
  postInstall
}

function postInstall {
  # Remove all archives
  echo "All programs installed, removing all archives now!"
  sleep 1
  rm -rfv ./*.tar.gz
  
  # Add alias.sh to .bashrc
  LINE='. /home/$USER/Documents/Development/alias.sh'
  FILE=/home/$USER/.bashrc
  grep -qF -- "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
  clear
  echo "Aliasses added. Please open a new terminal (or run 'bash' in this"
  echo "terminal) and try the following commands:"
  if [ -d "./PhpStorm" ]; then
      echo alias phpStorm="/home/$USER/Documents/Development/PhpStorm/bin/phpstorm.sh" >> alias.sh
    echo "phpStorm"
  fi
    if [ -d "./PyCharm" ]; then
    echo alias pyCharm="/home/$USER/Documents/Development/PyCharm/bin/pycharm.sh" >> alias.sh
    echo "pyCharm"
  fi
  if [ -d "./Rider" ]; then
    echo alias rider="/home/$USER/Documents/Development/Rider/bin/rider.sh" >> alias.sh
    echo "rider"
  fi
  echo ""
  echo "You can use the following server address to activate your downloads:"
  echo "- http://ssh.welizmusic.com:8000"
  echo ""
  echo "Have fun developing!"
}


###########################################
clear
checkDialog

installDevTools
