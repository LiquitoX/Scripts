#!/bin/bash

# This script will output the capacity of up to two
# batteries that are connected to this device.

# Get battery 1 (Internal)
echo "Internal battery (9 Cell):"
upower -i /org/freedesktop/UPower/devices/battery_BAT0 | \
grep -E "percentage|capacity"
echo ""

# Get battery 2 (External)
echo "External battery (9 Cell):"
upower -i /org/freedesktop/UPower/devices/battery_BAT1 | \
grep -E "percentage|capacity"

# End of script
