#!/bin/bash

# Declare variables
total_written=`sudo smartctl -A /dev/sda | grep LBAs_W | grep -o -E '[0-9]+$'`
total_read=`sudo smartctl -A /dev/sda | grep LBAs_R | grep -o -E '[0-9]+$'`
total_poh=`sudo smartctl -A /dev/sda | grep Power_On | grep -o -E '[0-9]+$'`

# Echo variables
echo '------------------------------'
echo 'SSD is located at /dev/sda'
echo '------------------------------'
echo 'Total Power On Hours:'
echo '  '$total_poh
echo 'Total LBAs written:'
echo '  '$total_written
echo 'Total LBAs read:'
echo '  '$total_read
echo '------------------------------'

# End of script.
