#!/bin/bash
###########
# This script is made to create an easy cronjob on all my machines.
# As constantly adding the complete command became painful to do.

# Please edit the letters below to correspond to your setup.
disks=("a" "b" "c" "d" "e" "f")

# Get the argument.
arg=${1}

# Function that shows the data.
function show {
  for item in ${disks[*]}; do
    smartctl -a  "/dev/sd"$item
  done
}

# Function that will start a long SMART test.
function testLong {
  for item in ${disks[*]}; do
    smartctl -t long  "/dev/sd"$item
  done
}

# Function that will start a short SMART test.
function testShort {
  for item in ${disks[*]}; do
    smartctl -t short  "/dev/sd"$item
  done
}

# Function that shows the last completed test per disk.
function tests {
  for item in ${disks[*]}; do
    echo "/dev/sd"$item
    smartctl -a "/dev/sd"$item | grep "# 1"
  done
}

# Function that shows the menu.
function help {
  clear
  echo "Help and usage:"
  echo "Usage: diskStatus -h"
  echo "-h   Shows this menu."
  echo "-a   Shows all data that you can GREP."
  echo "-t   Shows all the completed tests."
  echo "-tl  Will start a LONG test."
  echo "-ts  Will start a SHORT test."
}

# Process the argument and run appropriate task.
if [[ -z $arg  ]]; then
  help
  exit
elif [[ $arg = "-a" ]]; then
  show
  exit
elif [[ $arg = "-tl" ]]; then
  testLong
  exit
elif [[ $arg = "-ts" ]]; then
  testShort
  exit
elif [[ $arg = "-t" ]]; then
  tests
  exit
elif [[ $arg = "-h" ]]; then
  help
  exit
else
  echo "Argument is invalid!"
fi
